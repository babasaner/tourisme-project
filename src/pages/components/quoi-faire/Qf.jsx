import React from 'react'
import './qf.css'

const Qf = () => {
  return (
    <div className='quoi-Faire'>
        <div className="quoi-FaireItem">
            <img src="https://www.visitezlesenegal.com/wp-content/smush-webp/2020/12/destination-senegal-culture-scaled.jpg.webp" alt="Art & Culture" />
            <div className="quoi-FaireTitles">
                <h1>Art et Culture</h1>
            </div>
        </div>

        <div className="quoi-FaireItem">
            <img src="https://www.visitezlesenegal.com/wp-content/smush-webp/2021/01/thiebou-dieune.jpg.webp" alt="Gastronomie" />
            <div className="quoi-FaireTitles">
                <h1>Gastronomie</h1>
            </div>
        </div>

        <div className="quoi-FaireItem">
            <img src="https://www.visitezlesenegal.com/wp-content/smush-webp/2021/01/Shopping-bag.jpg.webp" alt="Shopping" />
            <div className="quoi-FaireTitles">
                <h1>Shopping</h1>
            </div>
        </div>

        <div className="quoi-FaireItem">
            <img src="https://www.visitezlesenegal.com/wp-content/smush-webp/2021/01/Park-Destination-Senegal.jpg.webp" alt="Parc Naturels" />
            <div className="quoi-FaireTitles">
                <h1>Parc Naturels</h1>
            </div>
        </div>
    </div>
  )
}

export default Qf