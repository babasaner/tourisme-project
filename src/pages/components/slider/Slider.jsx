import React from 'react'
import './slide.css'
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Slider from "react-slick";
const Slide = () => {

    const settings = {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 3,
        slidesToScroll: 1,
        arrows: true,
        className: "monslider"
      };
  return (
    <div style={{"display":"flex","justifyContent":"center"} }>
        <Slider className='Caroussel' {...settings}>
        <div className='Slider'>
        <div className="Slideritem">
            <img src="https://www.visitezlesenegal.com/wp-content/uploads/2023/06/banniere1930-x-1080px-3.jpg" alt="M'bour" className='pListImg' /> 
        </div>
        </div>

        <div className='Slider'>
        <div className="Slideritem">
            <img src="https://www.visitezlesenegal.com/wp-content/uploads/2023/06/banniere-taamu-sn.jpg" alt="M'bour" className='pListImg' /> 
        </div>
        </div>

        <div className='Slider'>
        <div className="Slideritem">
            <img src="https://www.visitezlesenegal.com/wp-content/uploads/2023/06/banniere_taamu_senegal.jpg" alt="M'bour" className='pListImg' /> 
        </div>
        </div>

        <div className='Slider'>
        <div className="Slideritem">
            <img src="https://www.visitezlesenegal.com/wp-content/uploads/2023/06/banniere1930-x-1080px-3.jpg" alt="M'bour" className='pListImg' /> 
        </div>
        </div>

        <div className='Slider'>
        <div className="Slideritem">
            <img src="https://www.visitezlesenegal.com/wp-content/uploads/2023/06/banniere1930-x-1080px-3.jpg" alt="M'bour" className='pListImg' /> 
        </div>
        </div>
      </Slider>
    </div>
  )
}

export default Slide