import React from 'react'
import './events.css'

const Events = () => {
  return (
    <div className='evenements'>
        <div className="evenements-Item">
            <img src="http://www.visitezlesenegal.com/wp-content/uploads/2021/02/defile-Destination-Senegal.jpg" alt="Festival" />
            <div className="evenements-Titles">
                <h1>Festivals</h1>
            </div>
        </div>

        <div className="evenements-Item">
            <img src="https://www.visitezlesenegal.com/wp-content/smush-webp/2021/01/Concert-Destination-Senegal.jpg.webp" alt="Festival" />
            <div className="evenements-Titles">
                <h1>Concerts</h1>
            </div>
        </div>

        <div className="evenements-Item">
            <img src="https://www.visitezlesenegal.com/wp-content/smush-webp/2021/01/exposition-scaled.jpg.webp" alt="Festival" />
            <div className="evenements-Titles">
                <h1>Expositions</h1>
            </div>
        </div>

        <div className="evenements-Item">
            <img src="http://www.visitezlesenegal.com/wp-content/uploads/2021/01/De-vinci-Destination-Senegal.jpg" alt="Festival" />
            <div className="evenements-Titles">
                <h1>Expositions</h1>
            </div>
        </div>
    </div>
  )
}

export default Events