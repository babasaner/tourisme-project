import React from 'react'
import './city.css'
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Slider from "react-slick";
const City = () => {
  return (
    
    <div className='destination'>
      <div className="destinationItem">
        <img src="https://www.visitezlesenegal.com/wp-content/smush-webp/2021/11/DAKAR-POLE-scaled.jpg" alt="Dakar" />
        <div className="destinationTitles">

          <h1>Dakar</h1>
        </div>
      </div>


      <div className="destinationItem">
        <img src="https://www.visitezlesenegal.com/wp-content/smush-webp/2023/08/casamance-1.jpg.webp" alt="Dakar" />
        <div className="destinationTitles">

          <h1>Casamance</h1>
        </div>
      </div>

      <div className="destinationItem">
        <img src="https://www.visitezlesenegal.com/wp-content/smush-webp/2023/08/saint-louis-1.jpg.webp" alt="Dakar" />
        <div className="destinationTitles">

          <h1>Saint-louis</h1>
        </div>
      </div>


      <div className="destinationItem">
        <img src="https://www.visitezlesenegal.com/wp-content/smush-webp/2021/11/DAKAR-POLE-scaled.jpg" alt="Dakar" />
        <div className="destinationTitles">

          <h1>Petites cotes</h1>
        </div>
      </div>
    </div>
  )
}

export default City