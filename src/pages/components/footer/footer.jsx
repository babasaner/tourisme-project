import React from 'react'
import'./footer.css';
import Logo from '../navbar/logo.png'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import { faEnvelope, faMessage, faPhone } from '@fortawesome/free-solid-svg-icons';

const footer = () => {
  return (
  
    <footer>
        <div className='footer'>
        <div className="footerForm">
            <form action="">
                <div className="nom">
                    <input type="text" id='nom' placeholder='Prénom & Nom' name='nom'/>
                </div>
                <div className='email'>
                    <input type="email" name="email" id="email" placeholder='Email' />
                </div>
                <div className="envoyer">
                    <input type="button" id='envoyer' value="S'abonner" />
                </div>
            </form>
        </div>
        </div>

        <div style={{"width":"100%","backgroundColor":"#373A3C","display":"flex","justifyContent":"center","paddingTop":"5%"}}>

    <div className="copyright">
        <div className="copyrightItem">
            <img src={Logo} alt="Destination" />
        </div>

        <div className="copyrightItem">
           <ul>
            <li>L'esprit Téranga</li>
            <li>Quoi Faire</li>
            <li>Destinations</li>
            <li>Planifier Votre Voyage</li>
            <li>Destinations</li>
            <li>Expériences</li>
            <li>Taamu Sénégal</li>
            <li>Blog</li>
           </ul>
        </div>


        <div className="copyrightItem">
           <ul>
            <li>L'esprit Téranga</li>
            <li>Quoi Faire</li>
            <li>Destinations</li>
            <li>Planifier Votre Voyage</li>
            <li>Destinations</li>
            <li>Expériences</li>
            <li>Taamu Sénégal</li>
            <li>Blog</li>
           </ul>
        </div>

        <div className="copyrightItem">
           <ul>
            <li> <FontAwesomeIcon icon={faEnvelope} />&nbsp;Contact@aspt.sn</li>
            <li><FontAwesomeIcon icon={faPhone} />&nbsp;    +221 33 869 61 90</li>
            
           </ul>
        </div>
    </div>
    </div>

  
    </footer> 
    
    
    
  )
}

export default footer