import React from 'react'
import './planifier.css'

const planifier = () => {
  return (
    <div className='planifier'>
        <div className="planifierItem">
        <img src="https://www.visitezlesenegal.com/wp-content/smush-webp/2021/01/Dakar-Destination-Senegal-scaled.jpg.webp" alt="infos pratique" />
        <div className="planifierTitles">
            <h1>Informations Pratiques</h1>
            </div>
        </div>


        <div className="planifierItem">
        <img src="https://www.visitezlesenegal.com/wp-content/smush-webp/2021/05/5.jpg.webp" alt="infos pratique" />
        <div className="planifierTitles">
            <h1>Se rendre au Sénégal</h1>
            </div>
        </div>


        <div className="planifierItem">
        <img src="https://www.visitezlesenegal.com/wp-content/smush-webp/2021/06/passport-ds.jpg.webp" alt="infos pratique" />
        <div className="planifierTitles">
            <h1>Visa</h1>
            </div>
        </div>


        <div className="planifierItem">
        <img src="https://www.visitezlesenegal.com/wp-content/smush-webp/2021/01/Sante-Destination-Senegal.jpg.webp" alt="infos pratique" />
        <div className="planifierTitles">
            <h1>Santé & Sécurité</h1>
            </div>
        </div>

        
    </div>
  )
}

export default planifier