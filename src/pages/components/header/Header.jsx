import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import './header.css'
import 'react-date-range/dist/styles.css'; // main css file
import 'react-date-range/dist/theme/default.css'; // theme css file
import { faBed, faPlane,faCar,faTaxi, faPerson } from '@fortawesome/free-solid-svg-icons'
import { faCalendarDays } from '@fortawesome/free-regular-svg-icons'
import { DateRange } from 'react-date-range'
import { useState } from 'react'
import { format } from 'date-fns';
const Header = ({type}) => {
  const [openDate, setOpenDate] = useState(false) 
  const [date, setDate] = useState([
    {
      startDate: new Date(),
      endDate: new Date(),
      key: 'selection'
    }
  ]);
const [openOptions,setopenOptions] = useState(false)
const [options,setOptions] = useState({
adulte:1,
enfant:0,
chambre:1,
})

const handleOption = (name,operation)=>{
  setOptions(prev=>{
    return{
      ...prev,[name]:operation ==="i" ? options[name] +1 : options[name] -1,
    }
  })
}

  return (
    <div className='header'>
    <div className="headerContainer">
      <div className="headerList">
        <div className="headerListItem active">
        <FontAwesomeIcon icon={faBed} />
        <span>Hébergement</span>
        </div>
        <div className="headerListItem">
        <FontAwesomeIcon icon={faPlane} />
        <span>Compagnie aérienne</span>
        </div>
        <div className="headerListItem">
        <FontAwesomeIcon icon={faCar} />
        <span>Bus touristique</span>
        </div>
        <div className="headerListItem">
        <FontAwesomeIcon icon={faTaxi} />
        <span>Taxis</span>
        </div>
      </div>
     
        
        <h1 className='headerTitle' >Trouvez votre prochain séjour</h1>
      <p className="headerdesc">Recherchez des offres sur des hôtels, des hébergements indépendants et plus encore</p>
      
    
      </div>

     
    </div> 
    
  )
}

export default Header