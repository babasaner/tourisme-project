import React from 'react'
import './navbar.css'
import Logo from './logo.png'

const Navbar = () => {
  return (
    <div className='navbar'>
        <div className="navContainer">
            <span className='logo'><img src={Logo} alt="Destination sénégal" /></span>
            <div className="navItems">
              <button className="navButton etablissement">Ajouter votre établissement</button>
                <button className="navButton">Créer un compte</button>
                <button className="navButton">Se connecter</button>
            </div>
            </div>
            </div>
  )
}

export default Navbar