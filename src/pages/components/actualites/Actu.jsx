import React from 'react'
import './actu.css'

const Actu = () => {
  return (
    <div className='actualites'>
        <div className="actualitesItem">
            <img src="https://www.visitezlesenegal.com/wp-content/uploads/2023/08/AIRL0159-scaled.jpg" alt="Senegal grandeur nature" />
            <div className="actualitesTitles">
                <h1>Sénégal Grandeur Nature</h1>
            </div>
        </div>

        <div className="actualitesItem">
            <img src="https://www.visitezlesenegal.com/wp-content/uploads/2023/06/Stade-Abdoulaye-wade-1.jpg" alt="Stade du Sénégal" />
            <div className="actualitesTitles">
                <h1>L’attractivité du Sénégal pour les organisateurs d’événements sportifs</h1>
            </div>
        </div>
    </div>
  )
}

export default Actu