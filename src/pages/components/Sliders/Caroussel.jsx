import React from 'react'
import './caroussel.css'
import BackgroundSlider from 'react-background-slider';
import image1 from './Senegal-Oriental_edited-1.jpg';
import image2 from './casamance_edited-1.jpg';

const Caroussel = () => {
  return (
    <div className='monslide'>
        <div className="monslideItem"><img src={image1} alt="Senegal Oriental" />
        
        <button>Découvrir</button>
        </div>
        
    </div>
  
  )
}

export default Caroussel