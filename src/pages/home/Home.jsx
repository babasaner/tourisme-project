import React from 'react'
import './home.css';
import Navbar from '../components/navbar/Navbar';
import City from '../components/city/City';
import PropertyList from '../components/propertyList/propertyList';
import Qf from '../components/quoi-faire/Qf';
import Events from '../components/evenements/Events';
import Actu from '../components/actualites/Actu';
import Planifier from '../components/planifier/planifier';
import Footer from '../components/footer/footer';
import Slider from '../components/slider/Slider'
import Caroussel from '../components/Sliders/Caroussel';
const Home = () => {
  return (
    <div>
      <Navbar/>
      <Caroussel/>
      <div className="homeContainer">
      <h1 className='homeTitle'>Nos offres Tammu Sénégal</h1>
      <Slider/>
      <h1 className='homeTitle'>Découvrez nos destinations</h1>
    <City/>
    
      <h1 className='homeTitle'>Magazine les Echos de la Destination Sénégal</h1>
      <iframe src="https://online.pubhtml5.com/vigqh/prxk/" frameborder="0"></iframe>
      <h1 className='homeTitle'>Quoi faire</h1>
      <Qf/>
      </div> 
      <div className="video">
        <iframe  className='video' src="https://www.youtube.com/embed/0I1pFXurjr4?controls=1&rel=0&playsinline=0&modestbranding=0&autoplay=0&enablejsapi=1&origin=https%3A%2F%2Fwww.visitezlesenegal.com&widgetid=1" frameborder="0"></iframe>
        
</div>

<div className="homeContainer">
<h1 className='homeTitle'>Évènements</h1>
<Events/>
<h1 className='homeTitle'>Actualités</h1>
<Actu/>
<button className='navButton'>Voir plus</button>

<h1 className='homeTitle'>Planifiez Votre Voyage</h1>
<Planifier/>
</div>
<div className="homeContainer"><Footer/></div>


    </div>
  )
}

export default Home