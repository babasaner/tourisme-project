import React from 'react'
import Navbar from '../components/navbar/Navbar';
import Header from '../components/header/Header'

const Hotel = () => {
  return (
    <div><Navbar/><Header/></div>
  )
}

export default Hotel